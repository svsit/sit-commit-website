/**
 * Some JS to juice up the page
 */

/**
 * @param {HTMLelement} element
 * @param {Array} actions
 */
function createRouter (element, actions) {
    return function() {
        let route = window.location.href.replace(/^[^#]+#?/, '');

        if (route === '') {
            route = '/';
        }
        
        console.log('routing ' + route);
        let matched = actions.some((action) => {
            if (action[0].test(route)) {
                action[1](element, route);
                return true;
            }

            return false;
        });

        if (matched === false) {
            element.innerHTML = '<h1>Not found</h1>';
        }
    };
}

function blinkCursor(cursor_element, timeout) {
    if (cursor_element.className === 'white-cursor') {
        cursor_element.className = 'black-cursor';
    } else {
        cursor_element.className = 'white-cursor';
    }

    setTimeout(() => blinkCursor(cursor_element, timeout), 500);
}

function createCursor() {
    let cursor = document.createElement('span');
    blinkCursor(cursor);
    cursor.innerHTML = '&nbsp;';

    return cursor;
}
    

/**
 * @param {HTMLElement} element
 * @param {Array} characters
 * @param {Number} timeout
 */
function typeEffect(element, characters, timeout, callback, cursor = null) {
    if (cursor === null) {
        cursor = createCursor();
    } else {
        element.removeChild(cursor);
    }

    if (characters.length === 0) {
        callback();
        return;
    }
    
    element.innerHTML += characters.shift();
    element.appendChild(cursor);
    setTimeout(() => typeEffect(element, characters, timeout, callback, cursor), timeout);
}

// function appendDescriptionTo(element) {
//     element.innerHTML += `
// <h1> Hallo wat doe ik nu hier</h1>
// `;
//}

function eventLog(element) {
    element.innerHTML += `
<p>commit&nbsp;06a7073e0d22712c2e037ef52501fb2fa65eef48&nbsp;(origin/master)<br/>
Author:&nbsp;SE&nbsp;Commissie&nbsp;<softwarecommissie@svsit.nl><br/>
Date:&nbsp;&nbsp;&nbsp;Wed&nbsp;Dec&nbsp;19&nbsp;14:00:00&nbsp;2018&nbsp;+0100<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Initial commit: Beer&nbsp;+&nbsp;code<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Free&nbsp;beers<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Free&nbsp;snacks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Free&nbsp;code<br/>
</p>`;
}

function typeLine(element, text, callback) {
    let line = document.createElement('p');
    line.className = 'command';
    element.appendChild(line);

    let cursor = createCursor();
    line.appendChild(cursor);
    
    typeEffect(
        line,
        text.split(''),
        150,
        () => {
            setTimeout(() => callback(element), 500);
        },
        cursor
    );
}
    

function renderIndex(element) {
    let content_container = document.createElement('section');

    document.body.appendChild(content_container);

    typeLine(content_container, 'sit commit -am "Initial commit: Beer + code"', () => {
        content_container.innerHTML += `
<p>[master 20a72a0] Beer + code<br/>
2 files changed, 96 insertions(+), 59 deletions(-)</p>`;
        
        typeLine(content_container, 'sit push', () => {
            typeLine(content_container, 'sit log', () => {
                eventLog(content_container);
            });
        });
    });
}

var router = createRouter(
    document.body,
    [
        [ /^\/$/, renderIndex ],
    ]
);

window.addEventListener('popstate', router);
router();
